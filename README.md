# Batch fitting 1D profiles to Gaussians with offset

Batch fitting intensity profiles contained in .csv files.

Alistair Curd, University of Leeds, 23/02/2022

Copyright 2022 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

v0.0

## DEVELOPED WITH:
Python 3.9.7 and Miniconda (Anaconda, Inc.) on Windows 10.

Tested with Anaconda on Mac OS X.

## ENVIRONMENT:

The base environment in Anaconda (Anaconda, Inc.) will run the notebook.

Alternatively, you can install the much slimmer Miniconda and create a new environment to run the script, like this:

* Install Miniconda (Python 3) from https://docs.conda.io/en/latest/miniconda.html.
* Use the Anaconda Windows Prompt or Terminal/command line.
*  To see what conda environments you have, run ```conda env list```
* If you do not already have an environment called ```gauss1d```, run
    ```conda env create -f environment.yml```.
* To start using the environment, run ```conda activate gauss1d```
* To stop using that enviroment: ```conda deactivate```
* To remove the environment, if you no longer want to use batch_1DGaussFitWithOffset:
    ```conda remove --name gauss1d --all```

## USAGE:

Save 1D intensity (column 1) vs position (column 0) profiles as .csv files with comma or tab delimiters in an input folder.

Run the Jupyter notebook *batch_fit_1DGaussianWithOffset.ipynb*:

* Navigate to the folder containing this software in the Anaconda prompt.
* Run the command ```jupyter notebook```. This will open the Jupyter home page in a browser window.
* Open the notebook from that home page.
* Run cell by cell or 'Run All'. You will be prompted to select input and output folders and shown some example fits at the end of the notebook.

The notebook introduction and information underneath various cells give you information about the output and progress at different stages.

## EXAMPLE:
*batch_fit_1DGaussianWithOffset-WithOutput.ipynb* can also be loaded by running ```jupyter notebook``` from the Anaconda prompt, having navigated to the folder containing this code. It shows example output in the notebook when running *batch_fit_1DGaussianWithOffset.ipynb*.

## CONTACT
Alistair Curd
a.curd@leeds.ac.uk